import "../css/main.css";
import "../css/key.scss";
import "../css/octave.css";
import "../css/piano.css";
import musicSheet from "../music-sheets/opus.txt";
import { addPianoListeners, octaveSelection, octaveSelected } from "./piano.js";
import { readMusicSheet, playMusicSheet } from "./music-sheet.js";

octaveSelection(octaveSelected);
readMusicSheet(musicSheet);
addPianoListeners();

const play = document.getElementById("play");
const stop = document.getElementById("stop");
const file = document.getElementById("file");

let stopfn = function () {

};

play.onclick = function (event) {
    play.disabled = true;
    stop.style.visibility = "visible";
    stop.disabled = false;
    stopfn = playMusicSheet(function () {
        play.disabled = false;
        stop.style.visibility = "hidden";
    });
    event.preventDefault();
}

stop.onclick = function (event) {
    play.disabled = false;
    stop.disabled = true;
    stopfn();
    event.preventDefault();
}


file.addEventListener("change", handleFiles, false);

function handleFiles() {
    const fileList = this.files; /* now you can work with the file list */

    const file = fileList[0];
    const reader = new FileReader();

    reader.onload = function () {
        readMusicSheet(reader.result);
    }

    reader.readAsText(file);
}
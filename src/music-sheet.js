import "../css/music-sheet.css";
import { playKey } from "./piano.js";

export function readMusicSheet(musicSheet) {
    const musicSheetDOM = document.getElementById("music-sheet");
    musicSheetDOM.classList.add("music-sheet");
    const keys = musicSheet.split("\n");

    keys.forEach(key => {
        const keyDOM = document.createElement("li");
        keyDOM.classList.add("music-sheet-key");
        keyDOM.innerHTML = key;

        musicSheetDOM.appendChild(keyDOM);

    });
}

let actualKeyIndex = 0;

export function playMusicSheet(callback) {
    const musicSheetDOM = document.getElementById("music-sheet");
    const musicSheetKeysDOM = musicSheetDOM.childNodes;

    const interval = setInterval(function () {
        const musicSheetKeyDOM = musicSheetKeysDOM[actualKeyIndex];
        musicSheetKeyDOM.classList.add("play");
        const key = musicSheetKeyDOM.innerText;
        playKey(key, true);

        setTimeout(function () {
            playKey(key, false);
            musicSheetKeyDOM.classList.remove("play");
        }, 450);

        actualKeyIndex++;

        if (actualKeyIndex >= musicSheetKeysDOM.length) {
            clearInterval(interval);
            actualKeyIndex = 0;
            callback();
            return;
        }
    }, 500);


    return function stop() {
        clearInterval(interval);
    };

}